alter view dbo.vw_customer_logo
as
select c.AccountId, c.Name1, c.AccountNumber, c.FederalId,
g.GraphicAdLogoId, g.ThumbnailDataId,
b.BlobData
from graphicascustomer gc
inner join customer c		on c.accountid = gc.CustomerAccountId 
inner join GraphicAdLogo g	on g.GraphicAdLogoId = gc.GraphicAdLogoId
inner join ShBlobData b	on b.id = g.ThumbnailDataId

