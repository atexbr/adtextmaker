﻿namespace AdTextMaker.Desktop.Model
{
    public class Customer
    {
        public string AccountId { get; set; }
        public string AccountNumber { get; set; }
        public string Name1 { get; set; }
        public string GraphicAdLogoId { get; set; }

        public Customer(string accountId, string accountNumber, string name1, string graphicAdLogoId)
        {
            AccountId = accountId;
            AccountNumber = accountNumber;
            Name1 = name1;
            GraphicAdLogoId = graphicAdLogoId;
        }
    }
}
