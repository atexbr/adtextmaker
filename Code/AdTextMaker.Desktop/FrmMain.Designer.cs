﻿namespace AdTextMaker.Desktop
{
    partial class FrmMain
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            this.grpSuperStyle = new System.Windows.Forms.GroupBox();
            this.rdoSS2 = new System.Windows.Forms.RadioButton();
            this.rdoSS1 = new System.Windows.Forms.RadioButton();
            this.rdoSS0 = new System.Windows.Forms.RadioButton();
            this.grpBorder = new System.Windows.Forms.GroupBox();
            this.rdoB2 = new System.Windows.Forms.RadioButton();
            this.rdoB1 = new System.Windows.Forms.RadioButton();
            this.rdoB0 = new System.Windows.Forms.RadioButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnRed = new System.Windows.Forms.Button();
            this.btnBold = new System.Windows.Forms.Button();
            this.btnWhatsapp = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.txtS = new System.Windows.Forms.TextBox();
            this.txtT = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtText = new System.Windows.Forms.TextBox();
            this.grpRTFCode = new System.Windows.Forms.GroupBox();
            this.txtRTFCode = new System.Windows.Forms.TextBox();
            this.grpBase64Encode = new System.Windows.Forms.GroupBox();
            this.txtBase64Encode = new System.Windows.Forms.TextBox();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.grpBackgroundColor = new System.Windows.Forms.GroupBox();
            this.rdoBC2 = new System.Windows.Forms.RadioButton();
            this.rdoBC1 = new System.Windows.Forms.RadioButton();
            this.rdoBC0 = new System.Windows.Forms.RadioButton();
            this.grpXML = new System.Windows.Forms.GroupBox();
            this.chkPlainText = new System.Windows.Forms.CheckBox();
            this.btnPesquisar = new System.Windows.Forms.Button();
            this.txtOutput = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.chkGenerateXML = new System.Windows.Forms.CheckBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tsLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.cmbAlign = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cmbCustomer = new System.Windows.Forms.ComboBox();
            this.btnCustomerLogo = new System.Windows.Forms.Button();
            this.btnShowXML = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnMake = new System.Windows.Forms.Button();
            this.chkRedText = new System.Windows.Forms.CheckBox();
            this.grpSuperStyle.SuspendLayout();
            this.grpBorder.SuspendLayout();
            this.panel1.SuspendLayout();
            this.grpRTFCode.SuspendLayout();
            this.grpBase64Encode.SuspendLayout();
            this.grpBackgroundColor.SuspendLayout();
            this.grpXML.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpSuperStyle
            // 
            this.grpSuperStyle.BackColor = System.Drawing.Color.White;
            this.grpSuperStyle.Controls.Add(this.rdoSS2);
            this.grpSuperStyle.Controls.Add(this.rdoSS1);
            this.grpSuperStyle.Controls.Add(this.rdoSS0);
            this.grpSuperStyle.Location = new System.Drawing.Point(6, 6);
            this.grpSuperStyle.Name = "grpSuperStyle";
            this.grpSuperStyle.Size = new System.Drawing.Size(100, 91);
            this.grpSuperStyle.TabIndex = 0;
            this.grpSuperStyle.TabStop = false;
            this.grpSuperStyle.Text = "Super Style";
            // 
            // rdoSS2
            // 
            this.rdoSS2.AutoSize = true;
            this.rdoSS2.Location = new System.Drawing.Point(15, 65);
            this.rdoSS2.Name = "rdoSS2";
            this.rdoSS2.Size = new System.Drawing.Size(38, 17);
            this.rdoSS2.TabIndex = 3;
            this.rdoSS2.Tag = "2";
            this.rdoSS2.Text = "C2";
            this.rdoSS2.UseVisualStyleBackColor = true;
            this.rdoSS2.Click += new System.EventHandler(this.rdoSS2_Click);
            // 
            // rdoSS1
            // 
            this.rdoSS1.AutoSize = true;
            this.rdoSS1.Location = new System.Drawing.Point(15, 42);
            this.rdoSS1.Name = "rdoSS1";
            this.rdoSS1.Size = new System.Drawing.Size(38, 17);
            this.rdoSS1.TabIndex = 2;
            this.rdoSS1.Tag = "1";
            this.rdoSS1.Text = "C1";
            this.rdoSS1.UseVisualStyleBackColor = true;
            this.rdoSS1.Click += new System.EventHandler(this.rdoSS1_Click);
            // 
            // rdoSS0
            // 
            this.rdoSS0.AutoSize = true;
            this.rdoSS0.Checked = true;
            this.rdoSS0.Location = new System.Drawing.Point(15, 19);
            this.rdoSS0.Name = "rdoSS0";
            this.rdoSS0.Size = new System.Drawing.Size(35, 17);
            this.rdoSS0.TabIndex = 1;
            this.rdoSS0.TabStop = true;
            this.rdoSS0.Tag = "0";
            this.rdoSS0.Text = "SI";
            this.rdoSS0.UseVisualStyleBackColor = true;
            this.rdoSS0.Click += new System.EventHandler(this.rdoSS0_Click);
            // 
            // grpBorder
            // 
            this.grpBorder.BackColor = System.Drawing.Color.White;
            this.grpBorder.Controls.Add(this.rdoB2);
            this.grpBorder.Controls.Add(this.rdoB1);
            this.grpBorder.Controls.Add(this.rdoB0);
            this.grpBorder.Location = new System.Drawing.Point(112, 6);
            this.grpBorder.Name = "grpBorder";
            this.grpBorder.Size = new System.Drawing.Size(100, 91);
            this.grpBorder.TabIndex = 4;
            this.grpBorder.TabStop = false;
            this.grpBorder.Text = "Border";
            // 
            // rdoB2
            // 
            this.rdoB2.AutoSize = true;
            this.rdoB2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdoB2.ForeColor = System.Drawing.Color.Red;
            this.rdoB2.Location = new System.Drawing.Point(15, 65);
            this.rdoB2.Name = "rdoB2";
            this.rdoB2.Size = new System.Drawing.Size(48, 17);
            this.rdoB2.TabIndex = 3;
            this.rdoB2.Tag = "2";
            this.rdoB2.Text = "Red";
            this.rdoB2.UseVisualStyleBackColor = true;
            this.rdoB2.Click += new System.EventHandler(this.rdoB2_Click);
            // 
            // rdoB1
            // 
            this.rdoB1.AutoSize = true;
            this.rdoB1.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.rdoB1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdoB1.Location = new System.Drawing.Point(15, 42);
            this.rdoB1.Name = "rdoB1";
            this.rdoB1.Size = new System.Drawing.Size(57, 17);
            this.rdoB1.TabIndex = 2;
            this.rdoB1.Tag = "0";
            this.rdoB1.Text = "Black";
            this.rdoB1.UseVisualStyleBackColor = true;
            this.rdoB1.Click += new System.EventHandler(this.rdoB1_Click);
            // 
            // rdoB0
            // 
            this.rdoB0.AutoSize = true;
            this.rdoB0.Checked = true;
            this.rdoB0.ForeColor = System.Drawing.Color.Gray;
            this.rdoB0.Location = new System.Drawing.Point(15, 19);
            this.rdoB0.Name = "rdoB0";
            this.rdoB0.Size = new System.Drawing.Size(72, 17);
            this.rdoB0.TabIndex = 1;
            this.rdoB0.TabStop = true;
            this.rdoB0.Tag = "-1";
            this.rdoB0.Text = "No border";
            this.rdoB0.UseVisualStyleBackColor = true;
            this.rdoB0.Click += new System.EventHandler(this.rdoB0_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel1.Controls.Add(this.chkRedText);
            this.panel1.Controls.Add(this.btnRed);
            this.panel1.Controls.Add(this.btnBold);
            this.panel1.Controls.Add(this.btnWhatsapp);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.txtS);
            this.panel1.Controls.Add(this.txtT);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.txtText);
            this.panel1.Location = new System.Drawing.Point(6, 103);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(870, 70);
            this.panel1.TabIndex = 5;
            // 
            // btnRed
            // 
            this.btnRed.Image = global::AdTextMaker.Desktop.Properties.Resources.letter_A_red_20;
            this.btnRed.Location = new System.Drawing.Point(835, 9);
            this.btnRed.Name = "btnRed";
            this.btnRed.Size = new System.Drawing.Size(28, 27);
            this.btnRed.TabIndex = 8;
            this.btnRed.Tag = "1606";
            this.btnRed.UseVisualStyleBackColor = true;
            this.btnRed.Click += new System.EventHandler(this.btnRed_Click);
            // 
            // btnBold
            // 
            this.btnBold.Image = global::AdTextMaker.Desktop.Properties.Resources.format_text_bold_22;
            this.btnBold.Location = new System.Drawing.Point(835, 40);
            this.btnBold.Name = "btnBold";
            this.btnBold.Size = new System.Drawing.Size(28, 27);
            this.btnBold.TabIndex = 7;
            this.btnBold.Tag = "1606";
            this.btnBold.UseVisualStyleBackColor = true;
            this.btnBold.Click += new System.EventHandler(this.btnBold_Click);
            // 
            // btnWhatsapp
            // 
            this.btnWhatsapp.Image = global::AdTextMaker.Desktop.Properties.Resources.icons8_whatsapp_20;
            this.btnWhatsapp.Location = new System.Drawing.Point(471, 24);
            this.btnWhatsapp.Name = "btnWhatsapp";
            this.btnWhatsapp.Size = new System.Drawing.Size(28, 27);
            this.btnWhatsapp.TabIndex = 6;
            this.btnWhatsapp.Tag = "1606";
            this.btnWhatsapp.UseVisualStyleBackColor = true;
            this.btnWhatsapp.Click += new System.EventHandler(this.btnWhatsapp_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(468, 8);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(31, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Text:";
            // 
            // txtS
            // 
            this.txtS.Enabled = false;
            this.txtS.Location = new System.Drawing.Point(67, 35);
            this.txtS.Name = "txtS";
            this.txtS.Size = new System.Drawing.Size(307, 20);
            this.txtS.TabIndex = 4;
            // 
            // txtT
            // 
            this.txtT.Enabled = false;
            this.txtT.Location = new System.Drawing.Point(67, 9);
            this.txtT.Name = "txtT";
            this.txtT.Size = new System.Drawing.Size(307, 20);
            this.txtT.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(30, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Title:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 38);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "SubTitle:";
            // 
            // txtText
            // 
            this.txtText.Location = new System.Drawing.Point(505, 9);
            this.txtText.Multiline = true;
            this.txtText.Name = "txtText";
            this.txtText.Size = new System.Drawing.Size(324, 58);
            this.txtText.TabIndex = 0;
            // 
            // grpRTFCode
            // 
            this.grpRTFCode.BackColor = System.Drawing.Color.WhiteSmoke;
            this.grpRTFCode.Controls.Add(this.txtRTFCode);
            this.grpRTFCode.Location = new System.Drawing.Point(6, 274);
            this.grpRTFCode.Name = "grpRTFCode";
            this.grpRTFCode.Size = new System.Drawing.Size(515, 259);
            this.grpRTFCode.TabIndex = 8;
            this.grpRTFCode.TabStop = false;
            this.grpRTFCode.Text = "RTF Code";
            // 
            // txtRTFCode
            // 
            this.txtRTFCode.Location = new System.Drawing.Point(9, 19);
            this.txtRTFCode.Multiline = true;
            this.txtRTFCode.Name = "txtRTFCode";
            this.txtRTFCode.ReadOnly = true;
            this.txtRTFCode.Size = new System.Drawing.Size(500, 232);
            this.txtRTFCode.TabIndex = 0;
            // 
            // grpBase64Encode
            // 
            this.grpBase64Encode.BackColor = System.Drawing.Color.WhiteSmoke;
            this.grpBase64Encode.Controls.Add(this.txtBase64Encode);
            this.grpBase64Encode.Location = new System.Drawing.Point(527, 274);
            this.grpBase64Encode.Name = "grpBase64Encode";
            this.grpBase64Encode.Size = new System.Drawing.Size(349, 259);
            this.grpBase64Encode.TabIndex = 9;
            this.grpBase64Encode.TabStop = false;
            this.grpBase64Encode.Text = "Base64 Encode";
            // 
            // txtBase64Encode
            // 
            this.txtBase64Encode.Location = new System.Drawing.Point(13, 19);
            this.txtBase64Encode.Multiline = true;
            this.txtBase64Encode.Name = "txtBase64Encode";
            this.txtBase64Encode.ReadOnly = true;
            this.txtBase64Encode.Size = new System.Drawing.Size(329, 232);
            this.txtBase64Encode.TabIndex = 1;
            // 
            // grpBackgroundColor
            // 
            this.grpBackgroundColor.BackColor = System.Drawing.Color.White;
            this.grpBackgroundColor.Controls.Add(this.rdoBC2);
            this.grpBackgroundColor.Controls.Add(this.rdoBC1);
            this.grpBackgroundColor.Controls.Add(this.rdoBC0);
            this.grpBackgroundColor.Location = new System.Drawing.Point(218, 6);
            this.grpBackgroundColor.Name = "grpBackgroundColor";
            this.grpBackgroundColor.Size = new System.Drawing.Size(110, 91);
            this.grpBackgroundColor.TabIndex = 5;
            this.grpBackgroundColor.TabStop = false;
            this.grpBackgroundColor.Text = "Background Color";
            // 
            // rdoBC2
            // 
            this.rdoBC2.AutoSize = true;
            this.rdoBC2.Enabled = false;
            this.rdoBC2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdoBC2.ForeColor = System.Drawing.Color.Black;
            this.rdoBC2.Location = new System.Drawing.Point(15, 65);
            this.rdoBC2.Name = "rdoBC2";
            this.rdoBC2.Size = new System.Drawing.Size(70, 17);
            this.rdoBC2.TabIndex = 3;
            this.rdoBC2.Text = "Custom X";
            this.rdoBC2.UseVisualStyleBackColor = true;
            this.rdoBC2.Visible = false;
            this.rdoBC2.Click += new System.EventHandler(this.rdoBC2_Click);
            // 
            // rdoBC1
            // 
            this.rdoBC1.AutoSize = true;
            this.rdoBC1.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.rdoBC1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdoBC1.ForeColor = System.Drawing.Color.Black;
            this.rdoBC1.Location = new System.Drawing.Point(15, 42);
            this.rdoBC1.Name = "rdoBC1";
            this.rdoBC1.Size = new System.Drawing.Size(69, 17);
            this.rdoBC1.TabIndex = 2;
            this.rdoBC1.Tag = "3";
            this.rdoBC1.Text = "Custom 1";
            this.rdoBC1.UseVisualStyleBackColor = true;
            this.rdoBC1.Click += new System.EventHandler(this.rdoBC1_Click);
            // 
            // rdoBC0
            // 
            this.rdoBC0.AutoSize = true;
            this.rdoBC0.Checked = true;
            this.rdoBC0.ForeColor = System.Drawing.Color.Gray;
            this.rdoBC0.Location = new System.Drawing.Point(15, 19);
            this.rdoBC0.Name = "rdoBC0";
            this.rdoBC0.Size = new System.Drawing.Size(66, 17);
            this.rdoBC0.TabIndex = 1;
            this.rdoBC0.TabStop = true;
            this.rdoBC0.Tag = "-1";
            this.rdoBC0.Text = "No Color";
            this.rdoBC0.UseVisualStyleBackColor = true;
            this.rdoBC0.Click += new System.EventHandler(this.rdoBC0_Click);
            // 
            // grpXML
            // 
            this.grpXML.BackColor = System.Drawing.Color.White;
            this.grpXML.Controls.Add(this.chkPlainText);
            this.grpXML.Controls.Add(this.btnPesquisar);
            this.grpXML.Controls.Add(this.txtOutput);
            this.grpXML.Controls.Add(this.label4);
            this.grpXML.Controls.Add(this.chkGenerateXML);
            this.grpXML.Location = new System.Drawing.Point(335, 6);
            this.grpXML.Name = "grpXML";
            this.grpXML.Size = new System.Drawing.Size(541, 91);
            this.grpXML.TabIndex = 10;
            this.grpXML.TabStop = false;
            this.grpXML.Text = "XML Importer Integration";
            this.grpXML.Enter += new System.EventHandler(this.grpXML_Enter);
            // 
            // chkPlainText
            // 
            this.chkPlainText.AutoSize = true;
            this.chkPlainText.Location = new System.Drawing.Point(311, 27);
            this.chkPlainText.Name = "chkPlainText";
            this.chkPlainText.Size = new System.Drawing.Size(75, 17);
            this.chkPlainText.TabIndex = 26;
            this.chkPlainText.Text = "Text/Plain";
            this.chkPlainText.UseVisualStyleBackColor = true;
            this.chkPlainText.Visible = false;
            // 
            // btnPesquisar
            // 
            this.btnPesquisar.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnPesquisar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPesquisar.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPesquisar.Image = ((System.Drawing.Image)(resources.GetObject("btnPesquisar.Image")));
            this.btnPesquisar.Location = new System.Drawing.Point(502, 56);
            this.btnPesquisar.Name = "btnPesquisar";
            this.btnPesquisar.Size = new System.Drawing.Size(32, 28);
            this.btnPesquisar.TabIndex = 21;
            this.btnPesquisar.UseVisualStyleBackColor = false;
            this.btnPesquisar.Click += new System.EventHandler(this.btnPesquisar_Click);
            // 
            // txtOutput
            // 
            this.txtOutput.Location = new System.Drawing.Point(67, 62);
            this.txtOutput.Name = "txtOutput";
            this.txtOutput.Size = new System.Drawing.Size(429, 20);
            this.txtOutput.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(19, 65);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(42, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Output:";
            // 
            // chkGenerateXML
            // 
            this.chkGenerateXML.AutoSize = true;
            this.chkGenerateXML.Location = new System.Drawing.Point(22, 27);
            this.chkGenerateXML.Name = "chkGenerateXML";
            this.chkGenerateXML.Size = new System.Drawing.Size(163, 17);
            this.chkGenerateXML.TabIndex = 0;
            this.chkGenerateXML.Text = "Generate XML File for testing";
            this.chkGenerateXML.UseVisualStyleBackColor = true;
            this.chkGenerateXML.Click += new System.EventHandler(this.chkGenerateXML_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 539);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(884, 22);
            this.statusStrip1.TabIndex = 12;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tsLabel
            // 
            this.tsLabel.Name = "tsLabel";
            this.tsLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel2.Controls.Add(this.cmbAlign);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.cmbCustomer);
            this.panel2.Controls.Add(this.btnCustomerLogo);
            this.panel2.Controls.Add(this.btnShowXML);
            this.panel2.Controls.Add(this.btnClose);
            this.panel2.Controls.Add(this.btnMake);
            this.panel2.Location = new System.Drawing.Point(6, 179);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(870, 89);
            this.panel2.TabIndex = 25;
            // 
            // cmbAlign
            // 
            this.cmbAlign.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbAlign.FormattingEnabled = true;
            this.cmbAlign.Items.AddRange(new object[] {
            "Center",
            "Left",
            "Right"});
            this.cmbAlign.Location = new System.Drawing.Point(331, 44);
            this.cmbAlign.Name = "cmbAlign";
            this.cmbAlign.Size = new System.Drawing.Size(129, 21);
            this.cmbAlign.TabIndex = 30;
            this.cmbAlign.Visible = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(289, 47);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(33, 13);
            this.label6.TabIndex = 29;
            this.label6.Text = "Align:";
            this.label6.Visible = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(271, 16);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(54, 13);
            this.label5.TabIndex = 28;
            this.label5.Text = "Customer:";
            // 
            // cmbCustomer
            // 
            this.cmbCustomer.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCustomer.FormattingEnabled = true;
            this.cmbCustomer.Location = new System.Drawing.Point(331, 13);
            this.cmbCustomer.Name = "cmbCustomer";
            this.cmbCustomer.Size = new System.Drawing.Size(471, 21);
            this.cmbCustomer.TabIndex = 27;
            this.cmbCustomer.SelectedIndexChanged += new System.EventHandler(this.cmbCustomer_SelectedIndexChanged);
            // 
            // btnCustomerLogo
            // 
            this.btnCustomerLogo.Image = global::AdTextMaker.Desktop.Properties.Resources.icons8_adicionar_imagem_24;
            this.btnCustomerLogo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCustomerLogo.Location = new System.Drawing.Point(471, 37);
            this.btnCustomerLogo.Name = "btnCustomerLogo";
            this.btnCustomerLogo.Size = new System.Drawing.Size(136, 32);
            this.btnCustomerLogo.TabIndex = 26;
            this.btnCustomerLogo.Text = "Add Customer Logo";
            this.btnCustomerLogo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCustomerLogo.UseVisualStyleBackColor = true;
            this.btnCustomerLogo.Click += new System.EventHandler(this.btnCustomerLogo_Click);
            // 
            // btnShowXML
            // 
            this.btnShowXML.Enabled = false;
            this.btnShowXML.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnShowXML.Location = new System.Drawing.Point(143, 13);
            this.btnShowXML.Name = "btnShowXML";
            this.btnShowXML.Size = new System.Drawing.Size(99, 61);
            this.btnShowXML.TabIndex = 25;
            this.btnShowXML.Text = "&Show XML";
            this.btnShowXML.UseVisualStyleBackColor = true;
            this.btnShowXML.Click += new System.EventHandler(this.btnShowXML_Click);
            // 
            // btnClose
            // 
            this.btnClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Image = global::AdTextMaker.Desktop.Properties.Resources.saida;
            this.btnClose.Location = new System.Drawing.Point(808, 14);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(55, 58);
            this.btnClose.TabIndex = 24;
            this.btnClose.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnMake
            // 
            this.btnMake.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMake.Image = global::AdTextMaker.Desktop.Properties.Resources.icons8_serviços_48;
            this.btnMake.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMake.Location = new System.Drawing.Point(6, 13);
            this.btnMake.Name = "btnMake";
            this.btnMake.Size = new System.Drawing.Size(119, 61);
            this.btnMake.TabIndex = 7;
            this.btnMake.Text = "&Make Text";
            this.btnMake.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnMake.UseVisualStyleBackColor = true;
            this.btnMake.Click += new System.EventHandler(this.btnMake_Click);
            // 
            // chkRedText
            // 
            this.chkRedText.AutoSize = true;
            this.chkRedText.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkRedText.ForeColor = System.Drawing.Color.Red;
            this.chkRedText.Location = new System.Drawing.Point(380, 12);
            this.chkRedText.Name = "chkRedText";
            this.chkRedText.Size = new System.Drawing.Size(49, 17);
            this.chkRedText.TabIndex = 9;
            this.chkRedText.Text = "Red";
            this.chkRedText.UseVisualStyleBackColor = true;
            this.chkRedText.CheckedChanged += new System.EventHandler(this.chkRedText_CheckedChanged);
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(884, 561);
            this.Controls.Add(this.grpBorder);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.grpXML);
            this.Controls.Add(this.grpBackgroundColor);
            this.Controls.Add(this.grpBase64Encode);
            this.Controls.Add(this.grpRTFCode);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.grpSuperStyle);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(900, 600);
            this.Name = "FrmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Ad Text Maker";
            this.Load += new System.EventHandler(this.FrmMain_Load);
            this.grpSuperStyle.ResumeLayout(false);
            this.grpSuperStyle.PerformLayout();
            this.grpBorder.ResumeLayout(false);
            this.grpBorder.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.grpRTFCode.ResumeLayout(false);
            this.grpRTFCode.PerformLayout();
            this.grpBase64Encode.ResumeLayout(false);
            this.grpBase64Encode.PerformLayout();
            this.grpBackgroundColor.ResumeLayout(false);
            this.grpBackgroundColor.PerformLayout();
            this.grpXML.ResumeLayout(false);
            this.grpXML.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox grpSuperStyle;
        private System.Windows.Forms.RadioButton rdoSS2;
        private System.Windows.Forms.RadioButton rdoSS1;
        private System.Windows.Forms.RadioButton rdoSS0;
        private System.Windows.Forms.GroupBox grpBorder;
        private System.Windows.Forms.RadioButton rdoB2;
        private System.Windows.Forms.RadioButton rdoB1;
        private System.Windows.Forms.RadioButton rdoB0;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtText;
        private System.Windows.Forms.Button btnMake;
        private System.Windows.Forms.GroupBox grpRTFCode;
        private System.Windows.Forms.GroupBox grpBase64Encode;
        private System.Windows.Forms.TextBox txtRTFCode;
        private System.Windows.Forms.TextBox txtBase64Encode;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.GroupBox grpBackgroundColor;
        private System.Windows.Forms.RadioButton rdoBC2;
        private System.Windows.Forms.RadioButton rdoBC1;
        private System.Windows.Forms.RadioButton rdoBC0;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtS;
        private System.Windows.Forms.TextBox txtT;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox grpXML;
        internal System.Windows.Forms.Button btnPesquisar;
        private System.Windows.Forms.TextBox txtOutput;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox chkGenerateXML;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnShowXML;
        private System.Windows.Forms.ToolStripStatusLabel tsLabel;
        private System.Windows.Forms.Button btnWhatsapp;
        private System.Windows.Forms.CheckBox chkPlainText;
        private System.Windows.Forms.ComboBox cmbCustomer;
        private System.Windows.Forms.Button btnCustomerLogo;
        private System.Windows.Forms.ComboBox cmbAlign;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnRed;
        private System.Windows.Forms.Button btnBold;
        private System.Windows.Forms.CheckBox chkRedText;
    }
}

