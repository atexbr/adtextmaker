﻿using System;
using System.Windows.Forms;

namespace AdTextMaker.Desktop
{
    public partial class FrmBrowser : Form
    {
        public string url;

        public FrmBrowser()
        {
            InitializeComponent();
        }

        private void FrmBrowser_Load(object sender, EventArgs e)
        {
            webBrowser1.Navigate(this.url);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
