﻿using AdTextMaker.Desktop.Business;
using System;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Diagnostics;
using FAndradeTI.Util;
using FAndradeTI.Util.WinForms;

namespace AdTextMaker.Desktop
{
    public partial class FrmMain : Form
    {
        private string outputXML;
        private MyEnvironment env;

        public FrmMain()
        {
            InitializeComponent();

            WinReg.SubKey = "SOFTWARE\\" + Application.CompanyName + "\\" + Application.ProductName;

            env = new MyEnvironment();
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            //btnWhatsapp.Tag = "1606";
            this.Text = AppInfo.GetFullTitle();

            try
            {
                txtOutput.Text = WinReg.Read("OutputPath");
                chkGenerateXML.Checked = (WinReg.Read("GenerateXML") == "true");
            }
            catch { }

            InitCustomers();
        }

        private void InitCustomers()
        {
            cmbCustomer.DisplayMember = "Name1";
            cmbCustomer.ValueMember = "GraphicAdLogoId";
            cmbCustomer.DataSource = env.Customers;
            cmbCustomer.Refresh();
        }

        private void rdoBC0_Click(object sender, EventArgs e)
        {
            //var color = new Color();
            grpBackgroundColor.BackColor = Color.FromArgb(255, 255, 255);
            ClearOptions();
        }

        private void rdoBC1_Click(object sender, EventArgs e)
        {
            //var color = new Color();
            grpBackgroundColor.BackColor = Color.FromArgb(255, 255, 192);
            ClearOptions();
        }

        private void rdoBC2_Click(object sender, EventArgs e)
        {
            var ret = colorDialog1.ShowDialog();

            if (ret == DialogResult.OK)
            {
                // Set form background to the selected color.
                grpBackgroundColor.BackColor = colorDialog1.Color;
            }
            ClearOptions();
        }

        private bool ValidateFields()
        {
            if (chkGenerateXML.Checked)
            {
                if (String.IsNullOrEmpty(txtOutput.Text)) return false;
            }

            if (String.IsNullOrEmpty(txtText.Text)) return false;

            if (txtText.Text.Contains("<red>"))
            {
                if (!txtText.Text.Contains("</red>")) return false;

                if (txtText.Text.IndexOf("<red>") > txtText.Text.IndexOf("</red>")) return false;
            }
            return true;
        }

        private void btnMake_Click(object sender, EventArgs e)
        {
            ClearOptions();
            Cursor.Current = Cursors.WaitCursor;
            tsLabel.Text = "Processando...";

            if (!ValidateFields())
            {
                Cursor.Current = Cursors.Default;
                tsLabel.Text = string.Empty;
                MessageBox.Show("There are blank fields that are mandatory", "Required Fields are blank", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            var ret = string.Empty;

            var ss = grpSuperStyle.Controls.OfType<RadioButton>()
                .FirstOrDefault(n => n.Checked);

            var b = grpBorder.Controls.OfType<RadioButton>()
               .FirstOrDefault(n => n.Checked);

            var bgc = grpBackgroundColor.Controls.OfType<RadioButton>()
                .FirstOrDefault(n => n.Checked);

            int tagSS = int.Parse(ss.Tag.ToString());
            int tagBorder = int.Parse(b.Tag.ToString());
            int tagBGColor = int.Parse(bgc.Tag.ToString());

            using (var mng = new SuperStyleBusiness(tagSS, tagBorder, tagBGColor, txtT.Text, txtS.Text, txtText.Text, chkPlainText.Checked, chkRedText.Checked))
            {
                txtRTFCode.Text = mng.GetRTFCode();
                txtBase64Encode.Text = mng.GetBase64Encode();

                WinReg.Write("OutputPath", txtOutput.Text);
                WinReg.Write("GenerateXML", (chkGenerateXML.Checked ? "true" : "false"));

                if (chkGenerateXML.Checked)
                {

                    outputXML = mng.SaveDummyAd(txtOutput.Text);
                    tsLabel.Text = outputXML;

                    int state = 0;
                    var respFile = string.Empty;

                    while (state != 99)
                    {
                        switch (state)
                        {
                            case 0:
                                state = env.IsLoaded ? 1 : 90;
                                break;

                            case 1:
                                state = string.Equals(env.InputDir, txtOutput.Text) ? 2 : 90;
                                break;

                            case 2:
                                Process[] pname = Process.GetProcessesByName("AdbaseXMLImporter");
                                state = pname.Length != 0 ? 3 : 90;
                                break;

                            case 3:
                                respFile = outputXML.Replace(env.InputDir, env.OutputDir).Replace(".xml", "_resp.xml");
                                outputXML = outputXML.Replace(env.InputDir, env.RecoveryDir);

                                Stopwatch sw = new Stopwatch();
                                sw.Start();

                                long seconds = 30;
                                while (true)
                                {
                                    //  
                                    if (sw.ElapsedMilliseconds > seconds * 1000)
                                    {
                                        state = 7;
                                        break;
                                    }


                                    if (File.Exists(respFile))
                                    {
                                        state = 4;
                                        break;
                                    }
                                }
                                sw.Stop();
                                break;

                            case 4:
                                state = !mng.IsValidDummyAd(respFile) ? 5 : 6;
                                break;

                            case 5:
                                tsLabel.Text = "Erro na integração do XML";
                                btnShowXML.Text = "Show XML Return";
                                outputXML = respFile;
                                state = 90;
                                break;

                            case 6:
                                tsLabel.Text = outputXML;
                                btnShowXML.Text = "Show XML";
                                state = 90;
                                break;

                            case 7:
                                tsLabel.Text = "Tempo excedido na integração - verifique o XML Importer!";
                                btnShowXML.Enabled = false;
                                state = 99;
                                break;

                            case 90:
                                btnShowXML.Enabled = true;
                                state = 99;
                                break;
                        }
                    }
                }
                else
                {

                    btnShowXML.Enabled = true;
                }
            }
            Cursor.Current = Cursors.Default;
        }

        private void ClearOptions()
        {
            btnShowXML.Enabled = false;
            btnShowXML.Text = "Show XML";
            btnShowXML.Refresh();
            txtRTFCode.Text = string.Empty;
            grpRTFCode.Refresh();
            txtBase64Encode.Text = string.Empty;
            grpBase64Encode.Refresh();
            tsLabel.Text = string.Empty;
            btnCustomerLogo.Enabled = false;
            cmbCustomer.SelectedIndex = 0;
        }

        private void rdoSS0_Click(object sender, EventArgs e)
        {
            txtT.Enabled = false;
            txtS.Enabled = false;
            txtT.Text = txtS.Text = string.Empty;
            txtText.Text = "Texto do Super Estilo Básico";
            ClearOptions();
        }

        private void rdoSS1_Click(object sender, EventArgs e)
        {
            txtT.Enabled = true;
            txtS.Enabled = false;
            txtT.Text = "Título C1";
            txtS.Text = string.Empty;
            txtText.Text = "Texto do Super Estilo C1";
            ClearOptions();
        }

        private void rdoSS2_Click(object sender, EventArgs e)
        {
            txtT.Enabled = true;
            txtS.Enabled = true;
            txtT.Text = "Título C2";
            txtS.Text = "Subtítulo C2";
            txtText.Text = "Texto do Super Estilo C2";
            ClearOptions();
        }

        private void btnPesquisar_Click(object sender, EventArgs e)
        {
            using (FolderBrowserDialog fbd = new FolderBrowserDialog
            {
                SelectedPath = txtOutput.Text
            })
            {
                DialogResult result = fbd.ShowDialog();

                if (result == DialogResult.OK)
                    txtOutput.Text = fbd.SelectedPath;
            }
        }

        private void chkGenerateXML_Click(object sender, EventArgs e)
        {
            //txtOutput.Text = "";
            txtOutput.Enabled = chkGenerateXML.Checked;
            btnPesquisar.Enabled = chkGenerateXML.Checked;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void rdoB0_Click(object sender, EventArgs e)
        {
            grpRTFCode.BackColor = Color.WhiteSmoke;
            grpRTFCode.ForeColor = Color.Black;
            ClearOptions();
        }

        private void rdoB1_Click(object sender, EventArgs e)
        {
            grpRTFCode.BackColor = Color.Black;
            grpRTFCode.ForeColor = Color.White;
            ClearOptions();
        }

        private void rdoB2_Click(object sender, EventArgs e)
        {
            grpRTFCode.BackColor = Color.Red;
            grpRTFCode.ForeColor = Color.White;
            ClearOptions();
        }


        private void btnShowXML_Click(object sender, EventArgs e)
        {
            FrmBrowser frm = new FrmBrowser
            {
                url = outputXML
            };
            frm.ShowDialog();

            //var showXML = File.ReadAllText(outputXML);
            //MessageBox.Show(showXML, "Show XML", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        private void btnWhatsapp_Click(object sender, EventArgs e)
        {
            var tag = "<whatsapp/>";

            if (txtText.Text.Contains(tag)) return;

            txtText.Paste(tag);
        }

        private void btnCustomerLogo_Click(object sender, EventArgs e)
        {
            string logo = cmbCustomer.SelectedValue.ToString();
            if (string.IsNullOrEmpty(logo)) return;
            txtText.Text += $" \\pard \\qc \\sl0\\logo{logo} \\par ";
        }

        private void cmbCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbCustomer.SelectedIndex == 0)
                btnCustomerLogo.Enabled = false;
            else
                btnCustomerLogo.Enabled = true;

        }

        private void btnRed_Click(object sender, EventArgs e)
        {
            InsertTag("<red>");
        }

        private void btnBold_Click(object sender, EventArgs e)
        {
            InsertTag("<bold>");
        }

        private void InsertTag(string tag)
        {
            if (txtText.Text.Contains(tag)) return;

            var selLength = txtText.SelectionLength;

            if (selLength == 0) return;

            var tag2 = tag.Insert(1,"/");

            var selStart = txtText.SelectionStart;

            var selected = txtText.Text.Substring(0, selStart) + tag + txtText.SelectedText + tag2 + txtText.Text.Substring(selStart + selLength, txtText.TextLength - (selStart + selLength));

            txtText.Text = selected;
        }

        private void grpXML_Enter(object sender, EventArgs e)
        {

        }

        private void chkRedText_CheckedChanged(object sender, EventArgs e)
        {
            if (chkRedText.Checked)
            { 
                txtT.ForeColor = Color.Red;
                txtS.ForeColor = Color.Red;
            }
            else
            {
                txtT.ForeColor = Color.Black;
                txtS.ForeColor = Color.Black;
            }
        }
    }
}
