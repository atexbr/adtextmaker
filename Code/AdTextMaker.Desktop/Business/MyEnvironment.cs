﻿using AdTextMaker.Desktop.Model;
using System.Collections.Generic;
using System.Configuration;
using System.IO;

namespace AdTextMaker.Desktop.Business
{
    public class MyEnvironment
    {
        private string inputDir;
        private string outputDir;
        private string recoveryDir;
        private bool isLoaded;
        private List<Customer> customers;

        public string InputDir { get => inputDir; set => inputDir = value; }
        public bool IsLoaded { get => isLoaded; set => isLoaded = value; }
        public string RecoveryDir { get => recoveryDir; set => recoveryDir = value; }
        public string OutputDir { get => outputDir; set => outputDir = value; }
        public List<Customer> Customers { get => customers; set => customers = value; }

        public MyEnvironment()
        {
            IsLoaded = false;
            Customers = new List<Customer>();
            Init();
            InitCustomers();
        }

        private void Init()
        {
            try
            {
                var configFile = ConfigurationManager.AppSettings["XMLImporterConfigFile"];
                if (!File.Exists(configFile))
                {
                    return;
                }

                using (StreamReader sr = new StreamReader(configFile))
                {
                    string line;
                    int count = 0;
                    while ((line = sr.ReadLine()) != null && count != 3)
                    {
                        GetFolder(line, "InputDir", ref inputDir, ref count);
                        GetFolder(line, "OutputDir", ref outputDir, ref count);
                        GetFolder(line, "RecoveryDir", ref recoveryDir, ref count);
                    }
                }

                IsLoaded = true;
            }
            catch
            {
                throw;
            }
        }

        private void InitCustomers()
        {
            Customers.Clear();
            Customers.Add(new Customer("0","0","-- Select a customer --", "0"));
            using (StreamReader sr = new StreamReader(@"templates\Customer.csv"))
            {
                string line;
                int count = 0;

                while ((line = sr.ReadLine()) != null && count != 3)
                {
                    var aux = line.Split(';');

                    Customers.Add(new Customer(aux[0], aux[1], aux[2], aux[3]));
                }
            }
        }
        private void GetFolder(string line, string cfgName, ref string cfg, ref int count)
        {
            if (line.Substring(0, cfgName.Length) == cfgName)
            {
                string[] items = line.Split('=');

                cfg = items[1];
                count++;
            }
        }

    }
}
