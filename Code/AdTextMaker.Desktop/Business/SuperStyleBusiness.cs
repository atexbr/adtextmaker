﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;

namespace AdTextMaker.Desktop.Business
{
    public class SuperStyleBusiness : IDisposable
    {
        protected readonly int superStyle;
        protected readonly int border;
        protected readonly int bgColor;
        protected readonly string title;
        protected readonly string subtitle;
        protected readonly string text;
        protected readonly bool isPlainText;
        protected readonly bool isRedText;
        protected string model;
        protected string modelEncoded;
        protected string[] outputModel;
        protected Dictionary<string, string> dicCode;
        protected Dictionary<string, string> dicTag;
        protected Dictionary<string, string> dicFormat;
        private readonly string logo = "1606";

        public SuperStyleBusiness(int superStyle, int border, int bgColor, string title, string subtitle, string text, bool isPlainText, bool isRedText)
        {
            this.superStyle = superStyle;
            this.border = border;
            this.bgColor = bgColor;
            this.title = title;
            this.subtitle = subtitle;
            this.text = text;
            this.isPlainText = isPlainText;
            this.isRedText = isRedText;
            this.dicCode = new Dictionary<string, string>();
            GenerateCodes();

            this.dicTag = new Dictionary<string, string>();
            dicTag.Add("<red>", @"templates\TextColor.txt");
            dicTag.Add("<bold>", @"templates\TextBold.txt");

            this.dicFormat = new Dictionary<string, string>();
            dicFormat.Add("<red>", "2");
            dicFormat.Add("<bold>", @"\b");

            outputModel = new string[] { "SI.txt", "C1.txt", "C2.txt" };
        }

        private string BuildModel()
        {
            var outputText = File.ReadAllText(@"templates\" + outputModel[this.superStyle], Encoding.UTF8);
            var header = File.ReadAllText(@"templates\Header.txt");

            outputText = outputText.Replace("[Header]", header);

            var b = File.ReadAllText(@"templates\Border.txt");

            if (this.border != -1)
                outputText = outputText.Replace("[Border]", b.Replace("[N]", this.border.ToString()));
            else
                outputText = outputText.Replace("[Border]", string.Empty);

            var bgc = File.ReadAllText(@"templates\BGColor.txt");

            if (this.bgColor != -1)
                outputText = outputText.Replace("[BGColor]", bgc.Replace("[N]", this.bgColor.ToString()));
            else
                outputText = outputText.Replace("[BGColor]", string.Empty);


            var managedText = ApplyColor(this.text);

            managedText = ApplyBold(managedText);

            managedText = ApplyWhatsapp(managedText);

            outputText = outputText.Replace("[Text]", GetCodedString(managedText));

            if (this.superStyle != 0)
                outputText = outputText.Replace("[Title]", GetCodedString(this.title));

            if (this.superStyle == 2)
                outputText = outputText.Replace("[SubTitle]", GetCodedString(this.subtitle));

            outputText = outputText.Replace("[N]", (isRedText ? "2" : "0"));

            return outputText;
        }

        public string ApplyWhatsapp(string str)
        {
            var tag = "<whatsapp/>";

            var posBegin = str.IndexOf(tag);

            if (posBegin == -1) return str;

            var ret = str.Replace(tag, $"\\sl0\\logo{this.logo} ");

            return ret;

        }


        public string ApplyFormat(string tag, string str)
        {
            var posBegin = str.IndexOf(tag);

            if (posBegin == -1) return str;

            posBegin += tag.Length;

            var tag2 = tag.Insert(1, "/");

            var posEnd = str.IndexOf(tag2);

            var selectedText = str.Substring(posBegin, posEnd - posBegin);

            var tc = File.ReadAllText(dicTag[tag]);

            var ret = tc.Replace("[Text]", selectedText);
            ret = ret.Replace("[N]", dicFormat[tag]);

            ret = str.Substring(0, posBegin - tag.Length) + ret + str.Substring(posEnd + tag.Length + 1, str.Length - (posEnd + tag.Length + 1));

            return ret;
        }


        private string ApplyColor(string str)
        {
            return ApplyFormat(dicTag.Keys.ElementAt(0), str);
        }

        private string ApplyBold(string str)
        {
            return ApplyFormat(dicTag.Keys.ElementAt(1), str);
        }

        public string GetRTFCode()
        {
            this.model = BuildModel();
            return this.model;
        }

        private void GenerateCodes()
        {
            string codes = @"\'e1\'e0\'e3\'e2\'e9\'ea\'ed\'f3\'f5\'f4\'fa\'fc\'c1\'c0\'c3\'c2\'c9\'ca\'cd\'d3\'d5\'d4\'da\'dc\'c7\'e7";
            string chars = "áàãâéêíóõôúüÁÀÃÂÉÊÍÓÕÔÚÜÇç";

            string[] c = codes.Split('\\');

            for (int i=0; i < chars.Length; i++)
            {
                dicCode.Add(chars.Substring(i, 1),  @"\" + c[i+1]);
            }
        }

        private string GetCodedString(string str)
        {
            var ret = str;

            foreach(string key in dicCode.Keys)
            {
                ret = ret.Replace(key, dicCode[key]);
            }

            return ret;
        }

        public string GetBase64Encode()
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(this.model);
            this.modelEncoded = System.Convert.ToBase64String(plainTextBytes, Base64FormattingOptions.None);
            return this.modelEncoded;
        }

        public string SaveDummyAd(string xmlDir)
        {
            var externalCode = DateTime.Now.ToString("yyyyMMdd-HHmmss");
            XmlDocument doc = new XmlDocument();
            doc.Load(@"templates\DummyAD.xml");
            doc.SelectSingleNode("/AdBaseData/AdBaseInfo/Ad/ad-type").InnerText = outputModel[this.superStyle].Substring(0, 2);
            if (!this.isPlainText)
            {
                doc.SelectSingleNode("/AdBaseData/AdBaseInfo/Ad/ad-content").InnerText = this.modelEncoded;
            }
            else
            {
                doc.SelectSingleNode("/AdBaseData/AdBaseInfo/Ad/ad-content").InnerText = this.title + " " + this.subtitle + " " + this.text;
                doc.SelectSingleNode("/AdBaseData/AdBaseInfo/Ad/ad-content/@type").Value = "text";
            }
            doc.SelectSingleNode("/AdBaseData/AdBaseInfo/Ad/AdLocInfo/ExternalRunSchedNumber").InnerText = "T" + externalCode.Replace("-", "");

            var ret = Path.Combine(xmlDir, externalCode + ".xml");

            doc.Save(ret);

            return ret;
        }

        public bool IsValidDummyAd(string xmlDir)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(xmlDir);
            var ret = doc.SelectSingleNode("/AdBaseData/AdBaseResponse/publication-code").InnerText;
            return ret != "N";
        }

        public void Dispose()
        {
            //throw new NotImplementedException();
        }

    }
}
